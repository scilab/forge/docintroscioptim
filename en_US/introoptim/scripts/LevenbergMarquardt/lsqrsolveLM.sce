// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

exec("lsqrsolveLM.sci");
exec("printsummary.sci");

//////////////////////////////////////////////////////////
//
// Test on Wood
//
mprintf("=======================\n")
mprintf("Test on Wood\n")
function fm = woodFM(x)
    fm(1) = 10*(x(2)-x(1)^2)
    fm(2) = 1-x(1)
    fm(3) = sqrt(90)*(x(4)-x(3)^2)
    fm(4) = 1-x(3)
    fm(5) = sqrt(10)*(x(2)+x(4)-2)
    fm(6) = (10)^(-1/2)*(x(2)-x(4))
endfunction

function [fm,J] = costfWood ( x )
    x = x(:)
    fm = woodFM(x)
    J = derivative(woodFM,x)
endfunction

function f = woodSS(x)
    fm = woodFM(x)
    f = 0.5*sum(fm.^2)
endfunction

// Check function value at initial guess
x0 = [-3 -1 -3 -1];
f = woodSS(x0);

[fm,J] = costfWood ( x0 );

// Check function value at x*
xstar = [1 1 1 1];
fstar = woodSS(xstar);

//
// Run Levenberg-Marquardt

maxiter = 100;
maxfevals = 100;
atolx = 0;
rtolx = 0;
atolg = 0;
verbose = %t;
[xopt,fopt,gopt,ropt,status,niter,nevalf] = lsqrsolveLM(costfWood,x0,maxiter,maxfevals,atolx,rtolx,atolg,verbose);

printsummary("LevenbergMarquardt",fopt,xopt,gopt,xstar,fstar,niter,nevalf,status);

//////////////////////////////////////////////////////////
//
// Test on Rosenbrock
//
mprintf("=======================\n")
mprintf("Test on Rosenbrock\n")
function fm = rosenbrock ( x )
    A = x(2) - x(1)^2
    B = 1-x(1)
    fm(1) = 10*A
    fm(2) = B
endfunction

function [fm,J] = costfRosen ( x )
    x = x(:)
    fm = rosenbrock(x)
    J = derivative(rosenbrock,x)
endfunction

x0 = [-1.2 1.0];
[fm,J] = costfRosen ( x0 )

xstar = [1 1];
[fm,J] = costfRosen ( xstar )
fstar = 0.5*sum(fm.^2);

//
// Run Levenberg-Marquardt

maxiter = 100;
maxfevals = 100;
atolx = 0;
rtolx = 0;
atolg = 0;
verbose = %t;
[xopt,fopt,gopt,ropt,status,niter,nevalf] = lsqrsolveLM(costfRosen,x0,maxiter,maxfevals,atolx,rtolx,atolg,verbose);

printsummary("LevenbergMarquardt",fopt,xopt,gopt,xstar,fstar,niter,nevalf,status);

/////////////////////////////////////////
mprintf("=======================\n")
mprintf("Testing Chemical equation\n")
function dy = chemicalModel ( t , y , a , b )
    // The right-hand side of the Ordinary Differential Equation.
    dy(1) = -a*y(2) + y(1) + t^2 + 6*t + b 
    dy(2) = b*y(1) - a*y(2) + 4*t + (a+b)*(1-t^2) 
endfunction 


function f = chemicalDifferences ( x, t, y_exp) 
    // Returns the difference between the simulated differential 
    // equation and the experimental data.
    a = x(1) 
    b = x(2) 
    y0 = y_exp(1,:)
    t0 = 0
    y_calc=ode(y0',t0,t,list(chemicalModel,a,b)) 
    diffmat = y_calc' - y_exp
    f = diffmat(:)
endfunction 

function [fm,J] = costfChemical ( x, t, y_exp )
    x = x(:)
    fm = chemicalDifferences(x, t, y_exp)
    J = derivative(list(chemicalDifferences, t, y_exp), x)
endfunction

//
// Experimental data 
t = [0 1 2 3 4 5 6]'; 
y_exp(:,1) = [-1 2 11 26 47 74 107]'; 
y_exp(:,2) = [ 1 3 09 19 33 51 73]'; 

// 
// Initial guess
a = 0.1; 
b = 0.4; 
x0 = [a;b]; 
[fm,J] = costfChemical ( x0, t, y_exp );

//
// Expected solution
xstar = [2;3];
[fm,J] = costfChemical ( xstar, t, y_exp );
fstar = 0.5*sum(fm.^2);


maxiter = 100;
maxfevals = 100;
atolx = 0;
rtolx = 0;
atolg = 0;
verbose = %t;
[xopt,fopt,gopt,ropt,status,niter,nevalf] = lsqrsolveLM(list(costfChemical,t,y_exp),x0,maxiter,maxfevals,atolx,rtolx,atolg,verbose);

printsummary("LevenbergMarquardt",fopt,xopt,gopt,xstar,fstar,niter,nevalf,status);

/////////////////////////////////////////
mprintf("=======================\n")
mprintf("Testing Problems from uncprb\n")

function [fm,J] = costfUncprb ( x, nprob, n, m )
    x = x(:)
    [fm,J]=uncprb_getfunc(n,m,x,nprob,3)
endfunction

passed = 0;
rtolF = 1.e-4;
atolF = 1.e-10;
rtolX = 1.e-4;
atolX = 1.e-6;
gopt = [];
geval = [];
nprobmax = uncprb_getproblems();
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    [fstar,xstar] = uncprb_getopt(nprob,n,m);
    maxiter = 1000;
    maxfevals = 1000;
    atolx = 0;
    rtolx = 0;
    atolg = 0;
    verbose = %f;
    [xopt,fopt,gopt,ropt,status,niter,nevalf] = lsqrsolveLM(list(costfUncprb,nprob,n,m),..
    x0,maxiter,maxfevals,atolx,rtolx,atolg,verbose);
    success = uncprb_printsummary("lsqrsolveLM", nprob, fopt, xopt, gopt, niter, nevalf, geval, ..
    rtolF, atolF, rtolX, atolX, "line");
    if (success) then
        passed = passed+1;
    end
end
mprintf("Total=%d\n",nprobmax);
mprintf("Passed=%d\n",passed);
mprintf("Failed=%d\n",nprobmax-passed);

//
// Manual tuning:
if ( %f ) then
    nprob = 3;
    [n,m,x0] = uncprb_getinitf(nprob);
    [fstar,xstar] = uncprb_getopt(nprob,n,m);
    maxiter = 500;
    maxfevals = 100;
    atolx = 0;
    rtolx = 0;
    atolg = 0;
    verbose = %t;
    [xopt,fopt,gopt,ropt,status,niter,nevalf] = lsqrsolveLM(list(costfUncprb,nprob,n,m),..
    x0,maxiter,maxfevals,atolx,rtolx,atolg,verbose);
    success = printsummary(msprintf("LevenbergMarquardt, Prob=#%d",nprob),..
    fopt,xopt,gopt,xstar,fstar,niter,nevalf,status);
end
