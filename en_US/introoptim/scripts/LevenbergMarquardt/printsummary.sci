// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function success = printsummary(solvername,fopt,xopt,gopt,xstar,fstar,niter,nevalf,status)
    mprintf("%s:\n",solvername);
    mprintf("  Iterations: %d\n", niter);
    mprintf("  Function evaluations: %d\n", nevalf);
    mprintf("  Status: %s\n", status);
    mprintf("  f(xopt)= %s (f(x*)=%s)\n", string(fopt), string(fstar));
    mprintf("  ||g(xopt)||= %s\n",string(norm(gopt)))
    xopt = xopt(:)
    xstar = xstar(:)
    if ( xstar <> [] ) then
        mprintf("  Absolute error on X: %s\n", string(norm(xopt-xstar)));
    end
    mprintf("  Absolute error on F: %s\n", string(abs(fopt-fstar)));
    success = ( fstar == fopt )
    if ( success ) then
        mprintf("  Success\n");
    else
        mprintf("  Failure\n");
    end
endfunction

