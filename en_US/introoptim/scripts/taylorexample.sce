// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = mypoly(x)
    y = 2*x.^2 + 5*x.^3 - 4 *x.^4
endfunction
function y = mypolyD(x)
    y = 4*x + 15*x.^2 - 16 *x.^3
endfunction
function y = mypolyDD(x)
    y = 4 + 30*x - 48 *x.^2
endfunction

N = 100;
x = linspace(0.5,1.5,N);
y = mypoly(x);
x0 = 1;
p = linspace(-0.5,0.5,N);
hx0 = mypoly(x0)
hpx0 = mypolyD(x0)
hppx0 = mypolyDD(x0)
yT = hx0 + hpx0.*p + 0.5 * hppx0.*p.^2;
plot(x,y,"b-")
plot(x0+p,yT,"r-")
legend(["2x^2+5x^3-4x^4" "Taylor 2nd Degree"]);
xtitle("Taylor formula","X","");
h = gcf();
h.children(1).children(1).legend_location="in_lower_right";
