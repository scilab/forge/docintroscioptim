// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//**********************************************************
// Draw the contours of a function associated for which the 
// optimization problem is easy
//
function f = myfunction ( x1 , x2 )
  f = x1**2.5 + (x2+1)**2;
endfunction
x = linspace(0,1,100);
y = linspace(-3,1,100);
contour ( x , y , myfunction , [0.1 0.5 1 2 3])
xs2png(0,"introoptim-easyproblem.png")

//**********************************************************
// Solve the original problem with optim

function [ f , g , ind ] = cost ( x , ind )
  f = x(1)**2.5 + (x(2)+1)**2;
  g = [
    2.5 * x(1)**1.5
    2 * (x(2)+1)
    ];
  plot ( x(1) , x(2) , "*" )
endfunction

x = linspace(0,1,100);
y = linspace(-3,1,100);
contour ( x , y , myfunction , [0.1 0.5 1 2 3])

xopt = [
  0 
  -1
  ];
x0 = [
  0.8 
  0.5
  ];
plot(x0(1),x0(2),"rO")
plot(xopt(1),xopt(2),"rO")

binf = [
  0
  -%inf
  ];
bsup = [
  %inf
  %inf
  ];
[f,x] = optim ( cost , "b" , binf , bsup , x0 , imp = -1 )

// The EXACT result is produced with 21 calls to the objective function.

//**********************************************************
// Draw the contours of the modified function
//
function f = transformed ( w1 , w2 )
  f = (w1**2)**2.5 + (w2+1)**2;
endfunction
x = linspace(-1,1,100);
y = linspace(-3,1,100);
contour ( x , y , transformed , [0.1 0.5 1 2 3])
xs2png(0,"introoptim-difficultproblem.png")


//**********************************************************
// Solve the transformed problem with optim

xopt = [
  0 
  -1
  ];
x0 = [
  0.8 
  0.5
  ];

// Compute the hessian matrix at optimum
function f = transformed2 ( w )
  f = (w(1)**2)**2.5 + (w(2)+1)**2;
endfunction

[J , H ] = derivative(transformed2 , xopt , H_form='blockmat')

function [ f , g , ind ] = costtrans ( w , ind )
  f = (w(1)**2)**2.5 + (w(2)+1)**2;
  g = [
    5 * w(1)**4 
    2 * (w(2)+1)
    ];
endfunction

plot(x0(1),x0(2),"rO")
plot(xopt(1),xopt(2),"rO")
[f,x] = optim ( costtrans , x0 , "ar" , 10000 , 1000 , 0 , imp = 2 )

// With the transformed problem, 711 iterations are performed with 714 function
// calls until the gradient is zero. But w1 = 2.034634462078438194D-62 is not exactly zero.


