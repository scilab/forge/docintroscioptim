// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Draw some convex functions on R
x = linspace ( -5 , 5 , 1000 );
plot ( x , exp(x) , "b-" )
plot ( x , x^2 , "g--" )
legend ( [ "exp(x)" "x^2"] )
//
x = linspace ( 0.1 , 3 , 1000 );
plot ( x , -log(x) , "b-" )
plot ( x , x.*log(x) , "g--" )
legend ( [ "-log(x)" "x*log(x)"] )

